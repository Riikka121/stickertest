<form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" role="search">
  <label class="sr-only" for="s"><?php esc_html_e('Search', 'levelup'); ?></label>
  <div class="search-input">
    <input class="search-input__field" id="s" name="s" type="text" placeholder="<?php esc_attr_e('Search &hellip;', 'levelup'); ?>" value="<?php the_search_query(); ?>">
    <input class="search-input__submit btn" id="searchsubmit" name="submit" type="submit" value="<?php esc_attr_e('Search', 'levelup'); ?>">
  </div>
</form>