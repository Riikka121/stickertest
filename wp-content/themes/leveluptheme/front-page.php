<?php

get_header();

$kupla1 = get_field('kupla1');
$kupla2 = get_field('kupla2');
$kupla3 = get_field('kupla3');
$kupla4 = get_field('kupla4');
$kupla5 = get_field('kupla5');
$kapteeni = get_field('kapteeni');
$puhekupla1 = get_field('puhekupla1');
$puhekupla2 = get_field('puhekupla2');
$puhekupla3 = get_field('puhekupla3');

$toinen1 = get_field('toinen1');
$toinen2 = get_field('toinen2');
$toinen3 = get_field('toinen3');

$super = get_field('super');
$supereka = get_field('supereka');
$supertoka = get_field('supertoka');
$superkolmas = get_field('superkolmas');
$superekakuva = get_field('superekakuva');
$supertokakuva = get_field('supertokakuva');
$superkolmaskuva = get_field('superkolmaskuva');

$kolmas1 = get_field('kolmas1');
$kolmas2 = get_field('kolmas2');
$kolmas3 = get_field('kolmas3');
$kolmas4 = get_field('kolmas4');
$kolmas5 = get_field('kolmas5');
$kolmas6 = get_field('kolmas6');


$background2= get_field('background2');
$background1= get_field('background1');
$kummitus = get_field('kummitus');
$mustekala = get_field('mustekala');
$pupu = get_field('pupu');
?>




<div class="container">
    

             <div class="columns" id="eka"  style="background-image:url('<?php echo $background1['url']; ?>');" >
                        

                <div class="col" id="col-2" >
                
                 <!--tietokone näkymä yli 1000px  -->               
                    <div class="speechbubble">
                    <?php if($puhekupla1):?>
                    <img src="<?php echo $puhekupla1['sizes']['large'];?>">
                    <?php endif ?>

                    <div class="divtext">
                     <?php if ($kupla1) : ?>                                                                                                                                                    
                    <h1><?php echo $kupla1 ?></h1>
                    <?php endif ?>
                                              
                                    
                     <?php if ($kupla2) : ?>
                    <?php if ($kupla3) : ?>
                     <?php if ($kupla4) : ?>
                    <h1><?php echo $kupla2 ?>
                    <a href="#form"><span><?php echo $kupla3?></span></a>
                    <?php echo $kupla4?></h1>
                    <?php endif ?>
                    <?php endif ?>
                    <?php endif ?>
                    </div>  
                    
                    </div>
                  </div>            
  <!--puhekupla yläpuolella tableteilla  550 tai suurempi -->                 
                 
                 <div class="speechbubble2">
                    <?php if($puhekupla2):?>
                    <img src="<?php echo $puhekupla2['sizes']['large'];?>">
                    <?php endif ?> 
                
                    <div class="divtext">
                     <?php if ($kupla1) : ?>                                                                                                                                                    
                    <h1><?php echo $kupla1?></h1>
                    <?php endif ?>
                                              
                                    
                     <?php if ($kupla2) : ?>
                    <?php if ($kupla3) : ?>
                     <?php if ($kupla4) : ?>
                    <h1><?php echo $kupla2 ?>
                    <a href="#form"><span><?php echo $kupla3?></span></a>
                    <?php echo $kupla4?></h1>
                    <?php endif ?>
                    <?php endif ?>
                    <?php endif ?>
                    </div>     
                    </div>

 <!--Mobile näkymä ei puhekuplaa otsikko  -->                      
                            <div class="speechbubble3">      
                               <div class="befirst">
                               <?php if ($kolmas1) : ?>
                                <h2><?php echo $kolmas1 ?></h2>
                                <?php endif ?>
                             </div>
                   

                               
 <!--mobile näkymä linkki lomakkeeseen   -->     
                            <div class="jointoday">
                              
                                <a href="#form"><?php if($puhekupla3):?>
                                <img src="<?php echo $puhekupla3['sizes']['large'];?>"></a>
                                <?php endif ?>
                                
                               <div class="divtext3">
                                <a href="#form"><?php if ($kupla5) : ?>                                                                                                                                                    
                                <h1><?php echo $kupla5 ?></h1></a>
                                <?php endif ?>
                                </div>
                            </div>
                </div> 
            
                         <div class="col" id="col-2" style="background-image:url('<?php echo $pupu['url']; ?>');" > 
       
                                <div class="kuva1">
                                <?php if($kapteeni):?>
                                <img src="<?php echo $kapteeni['sizes']['large'];?>">
                                <?php endif ?>
                                </div>
                               
                          </div> 
            </div> 
 
           
                       
                        

                        <div class="columns" id="toka">

                                                 
                            <div class="col" id="col-1" >
                                                               
                               <div class="content2">
                                    <div>
                                        <?php if ($toinen1) : ?>
                                            <?php if ($toinen2) : ?>

                                       <h1><?php echo $toinen1 ?><br>
                                       <?php echo $toinen2?></h1>
                                       <?php endif ?>
                                       <?php endif ?>
                                    </div>
                                    <div>
                                        <?php if ($toinen3) : ?>
                                       <h4><?php echo $toinen3 ?></h4>
                                       <?php endif ?>

                                     
                                    </div>
                                </div>
                           
                               
                                <div class="content3">
                                   
                                    <div class="join">
                                    <?php if($pupu):?>
                                        <a href="#form"><img src="<?php echo $pupu['sizes']['thumbnail'];?>"></a>
                                    <?php endif ?>
                                       
                                </div>
                        </div>
                    </div>  
                </div> 
               
                        

        <div class="columns">
            
            <div class="col" id="col-1_1">

                    
            <div class="col" id="super">

                <div class="eka">
                    <?php if ($super) : ?>
                    <?php if ($supereka) : ?>
                    <h5><?php echo $super?><br>
                    <span><?php echo $supereka?></span></h5>
                    <?php endif ?>
                    <?php endif ?>
                </div>

                <div class="toka">
                    <?php if ($super) : ?>
                    <?php if ($supertoka) : ?>
                    <h5><?php echo $super?><br>
                    <span><?php echo $supertoka?></span></h5>
                    <?php endif ?>
                    <?php endif ?>
                </div>

                <div class="kolmas">
                    <?php if ($super) : ?>
                    <?php if ($superkolmas) : ?>
                    <h5><?php echo $super?><br>
                    <span><?php echo $superkolmas?></span></h5>
                    <?php endif ?>
                    <?php endif ?>
                </div>

            </div>


                        <div class="firstjoin">
                            <div>
                             <?php if ($kolmas1) : ?>
                                <h2><?php echo $kolmas1 ?></h2>
                                <?php endif ?>
                             </div>
                             <div>
                                <?php if ($kolmas2) : ?>
                                    <?php if ($kolmas3) : ?>
                                <h4><?php echo $kolmas2 ?><br>
                                <?php echo $kolmas3 ?></h4>
                                <?php endif ?>
                                <?php endif ?>
                            </div>
                            
                             <div>
                                <?php if ($kolmas4) : ?>
                                <?php if ($kolmas5) : ?>
                                <h4><span><?php echo $kolmas4 ?></span><?php echo $kolmas5 ?></h4>
                                
                                <?php endif ?>
                                <?php endif ?>
                             </div>
                             <div>
                                <?php if ($kolmas6) : ?>
                                <h2><?php echo $kolmas6 ?></h2>
                                <?php endif ?>
                             </div> 
                             <div class="mustekala">
                                <?php if($mustekala):?>
                                <img src="<?php echo $mustekala['sizes']['large'];?>">
                                <?php endif ?>
                            </div>
                        </div> 

                        <div class="mobiili">
                             <div>
                                <?php if ($kolmas6) : ?>
                                <h2><?php echo $kolmas6 ?></h2>
                                <?php endif ?>
                             </div> 
                             <div>
                                <?php if ($kolmas2) : ?>
                                    <?php if ($kolmas3) : ?>
                                <h4><?php echo $kolmas2 ?><br>
                                <?php echo $kolmas3 ?></h4>
                                <?php endif ?>
                                <?php endif ?>
                            </div>
                            <div>
                                <?php if ($kolmas4) : ?>
                                <?php if ($kolmas5) : ?>
                                <h4><span><?php echo $kolmas4 ?></span><?php echo $kolmas5 ?></h4>
                                
                                <?php endif ?>
                                <?php endif ?>
                             </div>

                        </div>
            </div>
        </div>
           
    

            <div class="columns" id="form">
                <div class="col"  id="col-4">
                   
<!--
                        <div class="col" id="super">
                        <div class="eka">
                        <?php if($joinkuva):?>
                        <img class="icon" src="<?php echo $joinkuva['sizes']['thumbnail'];?>">
                       <?php endif ?>
                        </div>
                       </div> 
-->
                        <div class="kummitus">
                            <?php if($kummitus):?>
                            <img src="<?php echo $kummitus['sizes']['large'];?>">
                            <?php endif ?>
                        </div>

                            <div><a id="form">
                                <?php
                                gravity_form( 1, true, false, false, '', true, 12 );
                                ?>
                            </a></div>

                           
                        
                             
                </div>                 
            </div>
               
</div>      
                           
                    <div class ="container" 
                        id="taustakuva"style="background-image:url('<?php echo $background1['url']; ?>');"> 
                    </div>   
                    
                   
       










<?php
get_footer();
?>
</body>
</html>