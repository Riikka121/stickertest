<?php

get_header();


do_action('before_main_content');

?>

<div class="container">

  <?php if (have_posts()) : ?>

    <header class="page-header">

      <h1 class="page-title">
        <?php
        printf(
          /* translators: %s: query term */
          esc_html__('Search Results for: %s', 'levelup'),
          '<span>' . get_search_query() . '</span>'
        );
        ?>
      </h1>

    </header>

  <?php while (have_posts()) : the_post();

      get_template_part('templates/partial/content');

    endwhile;

    the_posts_pagination();

  else :

    get_template_part('templates/partial/content', 'none');

  endif; ?>

</div>

<?php

do_action('after_main_content');

get_footer(); ?>