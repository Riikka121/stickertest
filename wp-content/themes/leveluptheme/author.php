<?php

get_header();

do_action('before_main_content');

?>

<div class="block-wrapper author-content">
  <div class="container">
  
    <header class="page-header author__header">
  
      <?php
      if (isset($_GET['author_name'])) {
        $current_author = get_user_by('slug', $author_name);
      } else {
        $current_author = get_userdata(intval($author));
      }
      ?>
  
      <h1><?php echo esc_html__('About:', 'levelup') . ' ' . esc_html($current_author->nickname); ?></h1>
  
      <?php if (!empty($current_author->ID)) : ?>
        <?php echo get_avatar($current_author->ID); ?>
      <?php endif; ?>
  
      <?php if (!empty($current_author->user_url) || !empty($current_author->user_description)) : ?>
        <dl>
          <?php if (!empty($current_author->user_url)) : ?>
            <dt><?php esc_html_e('Website', 'levelup'); ?></dt>
            <dd>
              <a href="<?php echo esc_url($current_author->user_url); ?>"><?php echo esc_html($current_author->user_url); ?></a>
            </dd>
          <?php endif; ?>
  
          <?php if (!empty($current_author->user_description)) : ?>
            <dt><?php esc_html_e('Profile', 'levelup'); ?></dt>
            <dd><?php esc_html_e($current_author->user_description); ?></dd>
          <?php endif; ?>
        </dl>
      <?php endif; ?>
  
      <h2><?php echo esc_html('Posts by', 'levelup') . ' ' . esc_html($current_author->nickname); ?>:</h2>
  
    </header>
  
    <ul>
  
      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
          <li>
            <?php
                printf(
                  '<a rel="bookmark" href="%1$s" title="%2$s %3$s">%3$s</a>',
                  esc_url(apply_filters('the_permalink', get_permalink($post), $post)),
                  esc_attr(__('Permanent Link:', 'levelup')),
                  the_title('', '', false)
                );
                ?>
            <?php \Levelup\get_posted_on(); ?>
            <?php the_category('&'); ?>
          </li>
        <?php endwhile; ?>
  
      <?php else : ?>
  
        <?php get_template_part('templates/partial/content', 'none'); ?>
  
      <?php endif; ?>
  
    </ul>
  </div>
</div>

<?php 

do_action('after_main_content');

get_footer();

?>