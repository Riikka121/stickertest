<?php

/*

Template name: Landing Page

*/

get_header('landing');

do_action('before_main_content');

while (have_posts()) : the_post();

  get_template_part('templates/partial/content', get_post_type());

endwhile;

do_action('after_main_content');

get_footer('landing');
