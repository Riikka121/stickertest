<?php

namespace Levelup;



function get_post_category_list($post_id)
{
  $category_list = get_the_category($post_id);

  if ($category_list) {
    echo '<div class="single-category-list">';
    echo '<p>' . __('Categories', 'levelup') . '</p>';
    echo '<ul>';
    foreach ($category_list as $category) {
      echo '<li>';
      echo '<a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
      echo '</li>';
    }
    echo '</ul>';
    echo '</div>';
  }
}




function archive_title($title)
{
  if (is_home()) {
    $title = get_the_title(get_option('page_for_posts', true));
  }

  return $title;
}
add_filter('get_the_archive_title', __NAMESPACE__ . '\archive_title');



function get_posted_on()
{
  $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
  if (get_the_time('U') !== get_the_modified_time('U')) {
    $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
  }
  
  $time_string = sprintf(
    $time_string,
    esc_attr(get_the_date('c')),
    esc_html(get_the_date()),
    esc_attr(get_the_modified_date('c')),
    esc_html(get_the_modified_date())
  );

  $posted_on   = apply_filters(
    'get_posted_on',
    sprintf(
      '<div class="posted-on"><p>%1$s</p><a href="%2$s" rel="bookmark">%3$s</a></div>',
      esc_html_x('Published', 'post date', 'levelup'),
      esc_url(get_permalink()),
      apply_filters('get_posted_on', $time_string)
    )
  );

  return $posted_on;
}
