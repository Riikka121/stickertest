<?php

namespace Levelup;

if(!is_woocommerce_activated()) {
  return;
}

add_action('after_setup_theme', __NAMESPACE__ . '\add_woocommerce_support');
/**
 * Declares WooCommerce theme support.
 */
function add_woocommerce_support()
{
  add_theme_support('woocommerce');

  // Add New Woocommerce 3.0.0 Product Gallery support.
  add_theme_support('wc-product-gallery-lightbox');
  add_theme_support('wc-product-gallery-zoom');
  add_theme_support('wc-product-gallery-slider');

  // hook in and customizer form fields.
  add_filter('woocommerce_form_field_args', __NAMESPACE__ . '\wc_form_field_args', 10, 3);
}


/**
 * Filter hook function monkey patching form classes
 * Author: Adriano Monecchi http://stackoverflow.com/a/36724593/307826
 *
 * @param string $args Form attributes.
 * @param string $key Not in use.
 * @param null   $value Not in use.
 *
 * @return mixed
 */
function wc_form_field_args($args, $key, $value = null)
{
  // Start field type switch case.
  switch ($args['type']) {
      /* Targets all select input type elements, except the country and state select input types */
    case 'select':
      // Add a class to the field's html element wrapper - woocommerce
      // input types (fields) are often wrapped within a <p></p> tag.
      $args['class'][] = 'form-group';
      // Add a class to the form input itself.
      $args['input_class']       = array('form-control', 'input-lg');
      $args['label_class']       = array('control-label');
      $args['custom_attributes'] = array(
        'data-plugin'      => 'select2',
        'data-allow-clear' => 'true',
        'aria-hidden'      => 'true',
        // Add custom data attributes to the form input itself.
      );
      break;
      // By default WooCommerce will populate a select with the country names - $args
      // defined for this specific input type targets only the country select element.
    case 'country':
      $args['class'][]     = 'form-group single-country';
      $args['label_class'] = array('control-label');
      break;
      // By default WooCommerce will populate a select with state names - $args defined
      // for this specific input type targets only the country select element.
    case 'state':
      // Add class to the field's html element wrapper.
      $args['class'][] = 'form-group';
      // add class to the form input itself.
      $args['input_class']       = array('', 'input-lg');
      $args['label_class']       = array('control-label');
      $args['custom_attributes'] = array(
        'data-plugin'      => 'select2',
        'data-allow-clear' => 'true',
        'aria-hidden'      => 'true',
      );
      break;
    case 'password':
    case 'text':
    case 'email':
    case 'tel':
    case 'number':
      $args['class'][]     = 'form-group';
      $args['input_class'] = array('form-control', 'input-lg');
      $args['label_class'] = array('control-label');
      break;
    case 'textarea':
      $args['input_class'] = array('form-control', 'input-lg');
      $args['label_class'] = array('control-label');
      break;
    case 'checkbox':
      $args['label_class'] = array('custom-control custom-checkbox');
      $args['input_class'] = array('custom-control-input', 'input-lg');
      break;
    case 'radio':
      $args['label_class'] = array('custom-control custom-radio');
      $args['input_class'] = array('custom-control-input', 'input-lg');
      break;
    default:
      $args['class'][]     = 'form-group';
      $args['input_class'] = array('form-control', 'input-lg');
      $args['label_class'] = array('control-label');
      break;
  } // end switch ($args).
  return $args;
}



function loop_columns()
{
  return 3; // 1 product per row
}
add_filter('loop_shop_columns', __NAMESPACE__ . '\loop_columns', 999);



remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5);



add_action('woocommerce_before_main_content', __NAMESPACE__ . '\woocommerce_product_wrapper_start', 10);

function woocommerce_product_wrapper_start()
{
  do_action('before_main_content');

  echo '<div class="container">';
  echo '<div class="woocommerce-wrapper">';
  echo '<div class="row">';

  echo '<div class="col-lg-9 order-2 product-loop-container" id="content">';
  echo '<div class="product-loop">';
}



add_action('woocommerce_after_main_content', __NAMESPACE__ . '\woocommerce_product_wrapper_end', 10);

function woocommerce_product_wrapper_end()
{
  echo '</div>';
  echo '</div>';

  echo '<div class="col-lg-3 order-1 sidebar-container">';
  get_template_part('templates/wc/sidebar', 'product-cat');
  echo '</div>';

  echo '</div>';
  echo '</div>';
  echo '</div>';

  do_action('after_main_content');
}



// add_action('before_main_content', __NAMESPACE__ . '\woocommerce_cart_wrapper_start', 10);

function woocommerce_cart_wrapper_start()
{
  if (is_woocommerce() || is_cart() || is_checkout() || is_account_page()) {
    echo '<div class="woocommerce-wrapper">';
    echo '<div class="container">';
  }
}



// add_action('after_main_content', __NAMESPACE__ . '\woocommerce_cart_wrapper_end', 10);

function woocommerce_cart_wrapper_end()
{
  if (is_woocommerce() || is_cart() || is_checkout() || is_account_page()) {
    echo '</div>';
    echo '</div>';
  }
}



function single_product_wrapper_start()
{
  echo '<div class="single-product-wrapper">';
}
// add_action('woocommerce_before_single_product_summary', __NAMESPACE__ . '\single_product_wrapper_start', 5);

function single_product_wrapper_end()
{
  echo '</div>';
}
// add_action('woocommerce_after_single_product', __NAMESPACE__ . '\single_product_wrapper_end', 10);



function product_content()
{
  echo '<div class="product-excerpt">';
  the_excerpt();
  echo '</div>';
}
add_action('woocommerce_after_shop_loop_item_title', __NAMESPACE__ . '\product_content', 7);



function product_loop_content_wrapper_start()
{
  echo '<div class="product-loop__content">';
}
add_action('woocommerce_before_shop_loop_item_title', __NAMESPACE__ . '\product_loop_content_wrapper_start', 20);



function product_loop_content_wrapper_end()
{
  echo '</div>';
}
add_action('woocommerce_after_shop_loop_item', __NAMESPACE__ . '\product_loop_content_wrapper_end', 1);



function related_products_args($args)
{
  $args['posts_per_page'] = 2;
  return $args;
}
add_filter('woocommerce_output_related_products_args', __NAMESPACE__ . '\related_products_args', 20);



function woocommerce_active()
{
  return class_exists('woocommerce');
}



add_filter( 'woocommerce_add_to_cart_fragments', __NAMESPACE__ . '\woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
			<?php $product_count = WC()->cart->get_cart_contents_count(); ?>
			<?php if ($product_count > 0) : ?>
			<span class="product_count"><?php echo $product_count; ?></span>
			<?php endif; ?>
	<?php

	$fragments['span.product_count'] = ob_get_clean();

	return $fragments;
}


/**
 * Patrik was here
 */
