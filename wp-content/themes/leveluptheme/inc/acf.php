<?php

namespace Levelup;

if (!function_exists('get_field')) {
  return;
}

function register_acf_block_types()
{
  $args = array(
    'icon'              => 'info',
    'render_callback'   => __NAMESPACE__ . '\render_block',
    'mode'              => 'edit',
    'supports' => array(
      'align' => false,
    ),
  );

  $blocks = array(
    array(
      'name'  => 'hero_slider',
      'title' => __('Hero Slider', 'levelup-admin'),
      'icon'  => 'format-gallery',
    ),
    array(
      'name'  => 'textblock',
      'title' => __('Textblock', 'levelup-admin'),
      'icon'  => 'editor-alignleft',
    ),
    array(
      'name'  => 'textblock_and_image',
      'title' => __('Textblock and Image', 'levelup-admin'),
      'icon'  => 'align-left',
    ),
    array(
      'name'  => 'large_image',
      'title' => __('Large image', 'levelup-admin'),
      'icon'  => 'format-image',
    ),
    // array(
    //   'name'  => 'map',
    //   'title' => __('Map', 'levelup-admin'),
    //   'icon'  => 'location',
    // ),
    array(
      'name'  => 'contacts',
      'title' => __('Contacts', 'levelup-admin'),
      'icon'  => 'admin-users',
    ),
    array(
      'name'  => 'contact_info',
      'title' => __('Contact info', 'levelup-admin'),
      'icon'  => 'admin-users',
    ),
    array(
      'name'  => 'button',
      'title' => __('Button', 'levelup-admin'),
      'icon'  => 'admin-users',
    ),
  );

  foreach($blocks as $block) {
    acf_register_block_type(array_merge($args, $block));
  }
}

add_action('acf/init', __NAMESPACE__ . '\register_acf_block_types');



function render_block($block)
{
  $slug = str_replace(
    array('acf/', '-'),
    array('', '_'),
    $block['name']
  );

?>

  <section class="<?php echo $block['id'] ?> block block-<?php echo $slug ?>">

    <?php if (file_exists(get_theme_file_path("/templates/blocks/{$slug}.php"))) {
      include(get_theme_file_path("/templates/blocks/{$slug}.php"));
    } ?>

  </section>

<?php
}



function button($button)
{
  if ((!$button['url'] && !$button['page']) || !$button['title']) {
    return;
  }

  $url = $button['type'] ? $button['page'] : $button['url'];

  if ($button['anchor']) {
    $url .= '#' . $button['anchor'];
  }

  ob_start();

?>

  <div>
    <a href="<?php echo $url ?>" class="button"><?php echo $button['title'] ?></a>
  </div>

<?php

  $return = ob_get_clean();

  return $return;
}



if (function_exists('acf_add_options_page')) {

  $option_page = acf_add_options_page(array(
    'page_title'   => 'Theme Settings',
    'menu_title'   => 'Theme Settings',
    'menu_slug'   => 'theme-settings',
    'capability'   => 'edit_posts',
    'redirect'   => false
  ));

  acf_add_options_sub_page(array(
    'page_title'   => 'Footer',
    'menu_title'   => 'Footer',
    'parent_slug'   => $option_page['menu_slug'],
  ));
}


add_action('acf/init', __NAMESPACE__ . '\acf_init_map_api');

function acf_init_map_api()
{
  $map_api_key = get_field('maps_api_key', 'option');

  acf_update_setting('google_api_key', $map_api_key);
}
