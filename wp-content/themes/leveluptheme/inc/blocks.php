<?php

namespace Levelup;

// function gutenberg_examples_01_register_block()
// {
//   wp_register_script(
//     'blocks',
//     \Levelup\get_theme_block_script_uri(),
//     array('wp-blocks', 'wp-element')
//   );

//   register_block_type('startertheme-yarn/example-03-editable-esnext', array(
//     'editor_script' => 'blocks',
//   ));
// }
// add_action('init', __NAMESPACE__ . '\gutenberg_examples_01_register_block');


// function gutenberg_examples_01_register_block()
// {
//   wp_register_script(
//     'blocks',
//     \Levelup\get_theme_block_script_uri(),
//     array('wp-blocks', 'wp-element', 'wp-editor')
//   );

//   register_block_type('startertheme-yarn/example-01-basic-esnext', array(
//     'editor_script' => 'blocks',
//     'render_callback' => __NAMESPACE__ . '\gutenberg_examples_dynamic_render_callback'
//   ));

//   register_block_type('startertheme-yarn/example-03-editable-esnext', array(
//     'editor_script' => 'blocks',
//   ));
// }
// add_action('init', __NAMESPACE__ . '\gutenberg_examples_01_register_block');





function gutenberg_examples_dynamic_render_callback($attributes, $content)
{
  $recent_posts = wp_get_recent_posts(array(
    'numberposts' => 1,
    'post_status' => 'publish',
  ));
  if (count($recent_posts) === 0) {
    return 'No posts';
  }

  $post = $recent_posts[0];
  $post_id = $post['ID'];

  return sprintf(
    '<a class="wp-block-my-plugin-latest-post" href="%1$s">%2$s</a>',
    esc_url(get_permalink($post_id)),
    esc_html(get_the_title($post_id))
  );
}
