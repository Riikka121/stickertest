<?php

namespace Levelup;



//disable admin new user registration email
if (!function_exists('wp_new_user_notification')) {
  function wp_new_user_notification($user_id, $deprecated = null, $notify = '')
  {
    return;
  }
}


//disable admin user password reset email
if (!function_exists('wp_password_change_notification')) {
  function wp_password_change_notification($user)
  {
    return;
  }
}

add_image_size('banner', 1920, 9999, true);
