<?php

namespace Levelup;



add_filter('render_block', __NAMESPACE__ . '\block_wrappers', 10, 2);
function block_wrappers($block_content, $block)
{
  if ((strpos($block['blockName'], 'acf/') === false && trim($block_content))) {
    if(is_woocommerce_activated() && !is_woocommerce()) {
      $block_content = '<div class="container">' . $block_content . '</div>';
    }

    $block_content = '<section class="block block-core">' . $block_content . '</section>';
  }

  return $block_content;
}



function get_disallowed_blocks()
{
  return array(
    'core/search',
    'core/buttons',
    'core/text-columns',
    'core/media-text',
    'core/more',
    'core/nextpage'
  );
}




add_filter('allowed_block_types', __NAMESPACE__ . '\allowed_block_types');

function allowed_block_types($allowed_blocks)
{
  $registered_blocks = \WP_Block_Type_Registry::get_instance()->get_all_registered();

  $disallowed_blocks = get_disallowed_blocks();

  foreach (array_keys($registered_blocks) as $block_name) {
    if (in_array($block_name, $disallowed_blocks)) {
      $registered_blocks[$block_name] = false;
    }
  }

  return $registered_blocks;
}



add_filter('gform_field_input', __NAMESPACE__ . '\add_placeholder', 10, 5);
function add_placeholder($input, $field, $value, $lead_id, $form_id)
{
  $field->placeholder = $field->label;
  $field->label = '';

  if ($field->isRequired) {
    $field->placeholder .= ' *';
  }

  return $input;
}



add_filter('gform_field_css_class', __NAMESPACE__ . '\gravityforms_custom_class', 10, 3);
function gravityforms_custom_class($classes, $field, $form)
{
  $classes .= ' gfield_' . $field->type . '_wrapper';

  return $classes;
}



add_action('before_main_content', __NAMESPACE__ . '\main_wrapper_before');

function main_wrapper_before()
{
  if (is_single()) {
    $class = get_post_type();
  } else if (is_search()) {
    $class = 'search';
  } else {
    $class = '';
  }

  echo "<div class='main-wrapper'>";
  echo "<div class='{$class}-wrapper'>";
}



add_action('after_main_content', __NAMESPACE__ . '\main_wrapper_after');

function main_wrapper_after()
{
  echo "</div>";
  echo "</div>";
}
