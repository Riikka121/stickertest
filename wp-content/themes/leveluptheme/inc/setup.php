<?php

namespace Levelup;


add_action('after_setup_theme', __NAMESPACE__ . '\startertheme_setup');

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function startertheme_setup()
{
  /*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on startertheme, use a find and replace
		 * to change 'levelup' to the name of your theme in all the template files
		 */
  load_theme_textdomain('levelup', get_template_directory() . '/lang');

  // Add default posts and comments RSS feed links to head.
  add_theme_support('automatic-feed-links');

  /*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
  add_theme_support('title-tag');

  // This theme uses wp_nav_menu() in one location.
  register_nav_menus(array(
    'primary' => __('Primary Menu', 'levelup'),
    'footer' => __('Footer Menu', 'levelup'),
  ));

  /*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
  add_theme_support('html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ));

  /*
		 * Adding Thumbnail basic support
		 */
  add_theme_support('post-thumbnails');

  /*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
  add_theme_support('post-formats', array(
    'aside',
    'image',
    'video',
    'quote',
    'link',
  ));

  // Set up the WordPress Theme logo feature.
  add_theme_support('custom-logo');

  // Add support for responsive embedded content.
  add_theme_support('responsive-embeds');



  add_action('wp_before_admin_bar_render', __NAMESPACE__ . '\remove_comments');

  function remove_comments()
  {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
  }



  // Removes from post and pages
  add_action('init', __NAMESPACE__ . '\remove_comment_support', 100);

  function remove_comment_support()
  {
    remove_post_type_support('post', 'comments');
    remove_post_type_support('page', 'comments');
  }



  add_action('upload_mimes', __NAMESPACE__ . '\add_file_types_to_uploads');

  function add_file_types_to_uploads($file_types)
  {

    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes);

    return $file_types;
  }



  // Filter custom logo with correct classes.
  add_filter('get_custom_logo', __NAMESPACE__ . '\change_logo_class');

  function change_logo_class($html)
  {

    $html = str_replace('class="custom-logo"', 'class="img-fluid"', $html);
    $html = str_replace('class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html);
    $html = str_replace('alt=""', 'title="Home" alt="logo"', $html);

    return $html;
  }



  add_filter('body_class', __NAMESPACE__ . '\body_classes');

  function body_classes($classes)
  {
    // Adds a class of group-blog to blogs with more than 1 published author.
    if (is_multi_author()) {
      $classes[] = 'group-blog';
    }
    // Adds a class of hfeed to non-singular pages.
    if (!is_singular()) {
      $classes[] = 'hfeed';
    }

    return $classes;
  }



  // Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
  add_filter('body_class', __NAMESPACE__ . '\adjust_body_class');

  function adjust_body_class($classes)
  {

    foreach ($classes as $key => $value) {
      if ('tag' == $value) {
        unset($classes[$key]);
      }
    }

    return $classes;
  }



  add_action('wp_head', __NAMESPACE__ . '\pingback');

  function pingback()
  {
    if (is_singular() && pings_open()) {
      echo '<link rel="pingback" href="' . esc_url(get_bloginfo('pingback_url')) . '">' . "\n";
    }
  }



  add_action('wp_head', __NAMESPACE__ . '\mobile_web_app_meta');

  function mobile_web_app_meta()
  {
    echo '<meta name="mobile-web-app-capable" content="yes">' . "\n";
    echo '<meta name="apple-mobile-web-app-capable" content="yes">' . "\n";
    echo '<meta name="apple-mobile-web-app-title" content="' . esc_attr(get_bloginfo('name')) . ' - ' . esc_attr(get_bloginfo('description')) . '">' . "\n";
  }
}



add_filter('mce_buttons_2', __NAMESPACE__ . '\tiny_mce_style_formats');

function tiny_mce_style_formats($styles)
{

  array_unshift($styles, 'styleselect');
  return $styles;
}


add_filter('tiny_mce_before_init', __NAMESPACE__ . '\tiny_mce_before_init');

function tiny_mce_before_init($settings)
{

  $style_formats = array(
    array(
      'title'    => 'Lead Paragraph',
      'selector' => 'p',
      'classes'  => 'lead',
      'wrapper'  => true,
    ),
    array(
      'title'  => 'Small',
      'inline' => 'small',
    ),
    array(
      'title'   => 'Blockquote',
      'block'   => 'blockquote',
      'classes' => 'blockquote',
      'wrapper' => true,
    ),
    array(
      'title'   => 'Blockquote Footer',
      'block'   => 'footer',
      'classes' => 'blockquote-footer',
      'wrapper' => true,
    ),
    array(
      'title'  => 'Cite',
      'inline' => 'cite',
    ),
  );

  if (isset($settings['style_formats'])) {
    $orig_style_formats = json_decode($settings['style_formats'], true);
    $style_formats      = array_merge($orig_style_formats, $style_formats);
  }

  $settings['style_formats'] = json_encode($style_formats);
  return $settings;
}



add_action('edit_category', __NAMESPACE__ . '\category_transient_flusher');
add_action('save_post', __NAMESPACE__ . '\category_transient_flusher');

function category_transient_flusher()
{
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return;
  }
  // Like, beat it. Dig?
  delete_transient('startertheme_categories');
}


add_action('admin_notices', __NAMESPACE__ . '\check_acf_installed');

function check_acf_installed()
{
  if (!is_plugin_active('advanced-custom-fields-pro/acf.php')) {

    $class = 'notice notice-error';
    $message = __('This theme requires ACF Pro to be active!');

    printf('<div class="%1$s"><p><strong>%2$s</strong></p></div>', esc_attr($class), esc_html($message));
  }
}



if (!function_exists('is_woocommerce_activated')) {
  function is_woocommerce_activated()
  {
    return class_exists('woocommerce');
  }
}
