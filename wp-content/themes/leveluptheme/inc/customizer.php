<?php

namespace Levelup;



/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
/**
 * Register basic customizer support.
 *
 * @param object $wp_customize Customizer reference.
 */
function customize_register($wp_customize)
{
  $wp_customize->get_setting('blogname')->transport         = 'postMessage';
  $wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
  $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';
}
add_action('customize_register', __NAMESPACE__ . '\customize_register');



/**
 * Register individual settings through customizer's API.
 *
 * @param WP_Customize_Manager $wp_customize Customizer reference.
 */
function theme_customize_register($wp_customize)
{

  /**
   * Select sanitization function
   *
   * @param string               $input   Slug to sanitize.
   * @param WP_Customize_Setting $setting Setting instance.
   * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
   */
  function theme_slug_sanitize_select($input, $setting)
  {

    // Ensure input is a slug (lowercase alphanumeric characters, dashes and underscores are allowed only).
    $input = sanitize_key($input);

    // Get the list of possible select options.
    $choices = $setting->manager->get_control($setting->id)->choices;

    // If the input is a valid key, return it; otherwise, return the default.
    return (array_key_exists($input, $choices) ? $input : $setting->default);
  }

  $wp_customize->add_setting(
    'startertheme_container_type',
    array(
      'default'           => 'container',
      'type'              => 'theme_mod',
      'sanitize_callback' => __NAMESPACE__ . '\theme_slug_sanitize_select',
      'capability'        => 'edit_theme_options',
    )
  );
}
add_action('customize_register', __NAMESPACE__ . '\theme_customize_register');

