<?php

namespace Levelup;



remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


add_action('wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_scripts');

function enqueue_scripts()
{
  $css_uri        = get_asset_file('main.css');
  $js_uri         = get_asset_file('main.js');
  $the_theme      = wp_get_theme();
  $theme_version  = $the_theme->get('Version');

  if ($css_uri) {
    wp_enqueue_style('theme', $css_uri, array(), $theme_version);
  }

  if ($js_uri) {
    wp_enqueue_script('theme-scripts', $js_uri, array(), $theme_version, true);
  }

  // if($block_js_uri) {
  //   wp_enqueue_script('admin-startertheme-scripts', $block_js_uri, array(), $theme_version, true);
  // }

  wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css', array());
  wp_enqueue_script('jquery');

  if (is_singular() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}




add_action('admin_enqueue_scripts', __NAMESPACE__ . '\enqueue_admin_scripts');

function enqueue_admin_scripts()
{
  $css_uri = get_asset_file('admin.css');

  if ($css_uri) {
    wp_enqueue_style('theme-admin-styles', $css_uri, array());
  }
}



function get_asset_file($filename)
{
  $file = get_stylesheet_directory() . "/dist/assets.json";

  if(!file_exists($file)) {
    return false;
  }

  $assets_file  = file_get_contents($file);
  $assets       = json_decode($assets_file, true);

  if (!$assets || !isset($assets[$filename])) {
    return false;
  }

  return get_template_directory_uri() . '/dist/' . $assets[$filename];
}
