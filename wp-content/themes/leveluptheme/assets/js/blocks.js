const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;
const { withSelect } = wp.data;
const { ServerSideRender } = wp.components;

const blockStyle = {
  backgroundColor: '#900',
  color: '#fff',
  padding: '20px',
};

// registerBlockType('startertheme-yarn/example-01-basic-esnext', {
//   title: 'Example: Basic (esnext)',
//   icon: 'universal-access-alt',
//   category: 'layout',
//   example: {},
//   edit() {
//     return <div style={blockStyle}>Hello World, step 1 (from the editor).</div>;
//   },
//   save() {
//     return <div style={blockStyle}>Hello World, step 1 (from the frontend).</div>;
//   },
// });


// registerBlockType('gutenberg-examples/example-03-editable-esnext', {
//   title: 'Example: Editable (esnext)',
//   icon: 'universal-access-alt',
//   category: 'layout',
//   attributes: {
//     content: {
//       type: 'array',
//       source: 'children',
//       selector: 'h1',
//     },
//   },
//   example: {
//     attributes: {
//       content: 'Hello World',
//     },
//   },
//   edit: (props) => {
//     const { attributes: { content }, setAttributes, className } = props;
//     const onChangeContent = (newContent) => {
//       setAttributes({ content: newContent });
//     };
//     return (
//       <RichText
//         tagName="h1"
//         className={className}
//         onChange={onChangeContent}
//         value={content}
//       />
//     );
//   },
//   save: (props) => {
//     return <RichText.Content tagName="h1" value={props.attributes.content} />;
//   },
// });




registerBlockType('startertheme-yarn/example-01-basic-esnext', {
  title: 'Example: last post',
  icon: 'megaphone',
  category: 'layout',
  attributes: {
    content: {
      type: 'array',
      source: 'children',
      selector: 'p'
    },
  },

  edit: function (props) {
    return (
      <ServerSideRender
        block="startertheme-yarn/example-01-basic-esnext"
        attributes={props.attributes}
      />
    );
  },
  // save: function (props) {
  //   return (
  //     <ServerSideRender
  //       block="startertheme-yarn/example-01-basic-esnext"
  //       attributes={props.attributes}
  //     />
  //   );
  // },
});
