<?php

namespace Levelup;



$startertheme_includes = array(
  'setup',
  'settings',
  'enqueue',
  'hooks',
  'extras',
  'customizer',
  'acf',
  'analytics',
  // 'blocks',
  'woocommerce',
);

foreach ($startertheme_includes as $file) {
  $filepath = locate_template('inc/' . $file . '.php');
  if (!$filepath) {
    trigger_error(sprintf('Error locating /inc%s for inclusion', $file), E_USER_ERROR);
  }
  require_once $filepath;
}
