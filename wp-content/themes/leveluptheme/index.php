<?php

get_header();

do_action('before_main_content');

?>


<div class="block-wrapper index-content">
  <div class="container">
    <div class="row">
      <div class="col-lg-9">

        <?php

        get_template_part('templates/partial/title');

        if (have_posts()) :

          while (have_posts()) : the_post();

            get_template_part('templates/partial/content', 'single');

          endwhile;

          the_posts_pagination();

        else :

          get_template_part('templates/partial/content', 'none');

        endif; ?>

      </div>

      <?php get_sidebar(); ?>
    </div>
  </div>
</div>

<?php 

do_action('after_main_content');

get_footer(); ?>