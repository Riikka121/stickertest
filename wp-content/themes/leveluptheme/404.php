<?php

get_header();

do_action('before_main_content');

?>

<div class="block-wrapper 404-content">
  <div class="container">
    <div class="error-404 not-found">
      <header class="page-header">
        <h1 class="page-title"><?php esc_html_e('Oops! That page can&rsquo;t be found.', 'levelup'); ?></h1>
      </header>

      <p><?php esc_html_e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'levelup'); ?></p>
    </div>
  </div>
</div>

<?php 

do_action('after_main_content');

get_footer();

?>