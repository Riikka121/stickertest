<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_template_part('templates/partial/head'); ?>


<body <?php body_class(); ?>>

  <style>
    body {
      font-family: 'Ramabhadra', sans-serif;
      font-size: 1.125rem;
      margin: 0;
      padding: 0;
    }

    @media (max-width: 1000px) {
      body {
        background-image: url('<?= get_field('background2')['url'] ?>');
      }
    }

    /*käytetty puhekuplassa, aina lila */
    h1 {
      font-family: 'Bangers', cursive;
      text-align: center;
      color: #271b57;
      letter-spacing: 0.05em;
      font-size: 3rem;
    }

    @media (max-width: 1500px) {
      h1 {
        font-size: 2.5rem;
        letter-spacing: 0.05em;
      }
    }

    @media (max-width: 1225px) {
      h1 {
        font-size: 2.2rem;
        letter-spacing: 0.07em;
      }
    }

    @media (max-width: 650px) {
      h1 {
        font-size: 2rem;
        letter-spacing: 0.06em;
      }
    }

    @media (max-width: 500px) {
      h1 {
        color: #271b57;
        letter-spacing: 0.06em;

      }
    }

    @media (max-width: 370px) {
      h1 {
        letter-spacing: 0.05em;
        font-size: 1.5rem;
        padding: 0;
      }
    }

    @media (max-width: 230px) {
      h1 {
        letter-spacing: 0;
        font-size: 1rem;
        padding: 0;
      }
    }

    /*puhekuplan korosteväri */
    h1 span {
      color: #F78880;
      font-size: 3.5rem;
      -webkit-text-stroke-width: .02rem;
      -webkit-text-stroke-color: #f78880;
    }

    @media (max-width: 1000px) {
      h1 span {
        color: #F78880;
        font-size: 2.5rem;
        -webkit-text-stroke-width: .02rem;
        -webkit-text-stroke-color: #f78880;
        letter-spacing: 0.07em;
      }
    }

    @media (max-width: 650px) {
      h1 span {
        color: #F78880;
        letter-spacing: 0.05em;
        font-size: 2rem;
        -webkit-text-stroke-width: 0.01rem;
        -webkit-text-stroke-color: #f78880;
      }
    }

    @media (max-width: 400px) {
      h1 span {
        font-size: 1.5rem;
        -webkit-text-stroke-width: 0;
        -webkit-text-stroke-color: #f78880;
      }
    }

    h2 {
      font-family: 'Bangers', cursive;
      text-align: center;
      color: #271b57;
      text-transform: uppercase;
      letter-spacing: 0.05em;
      font-size: 3rem;
    }

    @media (max-width: 1000px) {
      h2 {
        color: white;
        font-size: 2.2rem;
        letter-spacing: 0.02em;
      }
    }

    @media (max-width: 950px) {
      h2 {
        font-size: 2rem;
        letter-spacing: 0.02em;
      }
    }

    @media (max-width: 230px) {
      h2 {
        letter-spacing: 0;
        font-size: 1rem;
        padding: 0;
      }
    }

    h2 span {
      color: #f78880;
    }

    /*pinkki teksti*/
    h4 {
      font-family: 'Ramabhadra', sans-serif;
      font-style: italic;
      color: #f78880;
      text-align: center;
      font-size: 2rem;
      letter-spacing: 0;
    }

    @media (max-width: 500px) {
      h4 {
        font-size: 1.5rem;
      }
    }

    @media (max-width: 230px) {
      h4 {
        letter-spacing: 0;
        font-size: 1rem;
        padding: 0;
      }
    }

    /*enter memebership*/
    h4 span {
      color: #271b57;
      padding-right: 8px;
    }

    @media (max-width: 1000px) {
      h4 span {
        color: white;
      }
    }

    h5 {
      font-family: 'Bangers', cursive;
      text-align: center;
      color: #271b57;
      font-size: 35px;
    }

    h5 span {
      font-family: 'Bangers', cursive;
      text-align: center;
      color: #271b57;
      letter-spacing: 0.02em;
      font-weight: bold;
      font-size: 50px;
    }

    /*koko sivun container*/
    .container {
      width: 100%;
      max-width: 3000px;
      background-color: #a1d9da;
      margin: 0;
      padding: 0;
      overflow: hidden;
      justify-content: center;
      align-items: center;
      align-content: center;
    }


    @media (max-width: 1000px) {
      .container {
        margin: 3em auto;
        width: 80%;
        background-color: #271b57;
        padding-bottom: 2em;
        padding-top: 0;
        overflow: hidden;
        justify-content: center;
        align-items: center;
        align-content: center;
      }
    }

    @media (max-width: 500px) {
      .container {
        height: 100%;
        width: 100%;
        margin: 0;
        padding-bottom: 2em;
        padding-top: 3em;
      }
    }

    .columns {
      display: flex;
      justify-content: center;
      align-items: center;
      align-content: center;
      height: auto;
      margin: 0;
    }

    @media (max-width: 1000px) {
      .columns {
        width: 100%;
        overflow: hidden;
        display: flex;
        justify-content: center;
        align-items: center;
        align-content: center;
      }
    }

    /*YLIN OSA alhaalla hahmot taustakuvana*/
    #eka {
      overflow: hidden;
      position: relative;
      width: 100%;
      height: 1500px;
      background-position-y: 890px;
      background-size: auto;
      background-size: contain;
      background-repeat: no-repeat;
    }

    /*taustakuvaksi bodyssa esitelty kuva*/
    @media screen and (max-width: 1000px) {
      #eka {
        margin: 0;
        background-image: none !important;
        flex-direction: column;
        height: auto;
      }
    }

    /*kaksiosainen -ylinosuus tausta mustekala-hahmo*/
    #col-2 {
      width: 50%;
      position: relative;
      margin-bottom: 2em;
      background-repeat: no-repeat;
      background-size: 12em;
      background-position: left 75% top 100%;
    }

    @media (max-width:2100px) {
      #col-2 {
        background-image: none !important;
        margin-bottom: 0;
      }
    }

    @media (max-width:1000px) {
      #col-2 {
        width: 100%;
        background-image: none !important;
        margin-bottom: 0;
      }
    }

    /*yläosan 2.div captain sticker ja tautahahmo */
    .kuva1 {
      width: 750px;
      height: auto;
      align-items: center;
      margin-left: -10%;
      margin-top: -110px;
    }

    @media screen and (max-width: 2300px) {
      .kuva1 {
        width: 700px;
        margin-left: -10%;
        margin-top: -100px;
      }
    }

    @media screen and (max-width: 2000px) {
      .kuva1 {
        width: 650px;
      }
    }

    @media screen and (max-width: 1700px) {
      .kuva1 {
        width: 600px;
        height: auto;
        margin-left: -15%;
      }
    }

    @media screen and (max-width: 1500px) {
      .kuva1 {
        width: 550px;
        height: auto;
        margin-left: -20%;
      }
    }

    @media screen and (max-width: 1200px) {
      .kuva1 {
        width: 500px;
      }
    }

    @media screen and (max-width: 1000px) {
      .kuva1 {
        height: auto;
        width: 80%;
        align-items: center;
        padding-top: 0;
        margin-top: 0;
        margin-bottom: 80px;
        margin-left: 10%;
        padding-bottom: 5em;
      }
    }

    @media screen and (max-width: 400px) {
      .kuva1 {
        margin-bottom: 10px;
        padding-bottom: 2em;
      }
    }

    /*puhekupla div yläosassa*/
    /*näkymän vaihto puhekupla ylös 1000px*/
    .speechbubble2 {
      display: none;
    }

    /*näkyviin 550 mobiili ei puhekuplaa uusi otsikko*/
    .speechbubble3 {
      display: none;
    }

    /*1000+ koossa */
    .speechbubble {
      width: 750px;
      height: 600px;
      position: relative;
      top: -490px;
      left: 50%;
      display: inline-block;
    }

    @media screen and (max-width: 2315px) {
      .speechbubble {
        left: 45%;
      }
    }

    @media screen and (max-width: 2200px) {
      .speechbubble {
        top: -470px;
      }
    }

    @media screen and (max-width: 1960px) {
      .speechbubble {
        left: 40%;
      }
    }

    @media screen and (max-width: 1790px) {
      .speechbubble {
        left: 35%;
      }
    }

    @media screen and (max-width: 1700px) {
      .speechbubble {
        left: 20%;
        top: -450px;
      }
    }

    @media screen and (max-width: 1500px) {
      .speechbubble {
        width: 607px;
        height: 585px;
        left: 25%;
        top: -380px;
      }
    }

    @media screen and (max-width: 1400px) {
      .speechbubble {
        left: 10%;
      }
    }

    @media screen and (max-width: 1200px) {
      .speechbubble {
        left: 0%;
        top: -360px;
      }
    }

    @media screen and (max-width: 1000px) {
      .speechbubble {
        display: none;
      }
    }

    /*puhekuplan teksti h1*/
    .speechbubble .divtext {
      position: absolute;
      z-index: 999;
      margin: auto;
      left: 0;
      right: 2em;
      top: 30%;
      text-align: center;
      width: 50%;
    }

    @media screen and (max-width: 1500px) {
      .speechbubble .divtext {
        right: 1em;
      }
    }

    /*tabletilla kupla yläpuolella*/
    @media screen and (max-width: 1000px) {
      .speechbubble2 {
        width: 550px;
        height: 450px;
        position: relative;
        top: 0;
        display: inline-block;
        margin-bottom: 3em;
      }
    }

    @media screen and (max-width: 700px) {
      .speechbubble2 {
        width: 500px;
        height: 400px;
      }
    }

    @media screen and (max-width: 600px) {
      .speechbubble2 h1 {
        letter-spacing: 0.05em;
        font-size: 1.7rem;
      }
    }

    .speechbubble2 h1 span:hover {
      color: #a1d9da;
    }

    .speechbubble h1 span:hover {
      color: #a1d9da;
    }


    @media screen and (max-width: 600px) {
      .speechbubble2 {
        width: 400px;
        height: 300px;
      }
    }

    .speechbubble2 .divtext {
      position: absolute;
      z-index: 999;
      margin: 2em auto;
      left: 0;
      right: 0;
      top: 10%;
      text-align: center;
      width: 40%;
    }

    @media screen and (max-width: 550px) {
      .speechbubble2 {
        display: none;
      }
    }

    /*mobiili näkymä otsikko vaihtuu BE FIRST...*/
    @media screen and (max-width: 550px) {
      .speechbubble3 {
        display: inline-block;
      }
    }

    .befirst {
      width: 100%;
      text-align: center;
      padding: 2em;
    }

    /*mobiili näkymässä join today linkki-puhekupla lomakkeeseen*/
    .jointoday {
      display: none;
    }

    @media screen and (max-width: 550px) {
      .jointoday {
        display: inline-block;
        position: relative;
        width: 70%;
        margin-left: 15%;
        text-align: center;
        margin-bottom: 1em;
      }
    }

    .jointoday .divtext3 {
      position: absolute;
      z-index: 999;
      margin: auto;
      padding: 2em 0;
      left: 0;
      right: 0;
      top: 0;
      text-align: center;
      width: 100%;
    }

    .jointoday .divtext3 h1:hover {
      color: #a1d9da;
    }

    @media screen and (max-width: 360px) {
      .jointoday .divtext3 {
        padding: 1.7em 0;
      }
    }

    @media screen and (max-width: 260px) {
      .jointoday .divtext3 {
        padding: 1em 0;
      }
    }


    /*OSIO 2 VIISTO*/
    #col-1 {
      background: #271b57;
      transform: skew(0deg, -5deg) translateY(-120px);
      height: 600px;
    }

    @media (max-width:1300px) {
      #col-1 {
        height: 500px;
      }
    }

    @media (max-width:1000px) {
      #col-1 {
        width: 100%;
        height: 420px;
        background: white;
        transform: skew(0deg, -5deg) translateY(-120px);
      }
    }

    @media (max-width:890px) {
      #col-1 {
        height: 500px;
      }
    }

    @media (max-width:370px) {
      #col-1 {
        height: 480px;
      }
    }

    @media (max-width:230px) {
      #col-1 {
        height: 380px;
      }
    }

    /*mobiili näkymä*/
    .content3 {
      display: none;
    }

    /*tekstit osio 2*/
    .content2 {
      padding: 150px 200px;
      transform: skew(0deg, 5deg);
    }

    @media screen and (max-width:1300px) {
      .content2 {
        padding: 100px;
      }
    }

    @media screen and (max-width:1000px) {
      .content2 {
        padding: 0;
        margin: 0;
        transform: none;
        transform: skew(0deg, 5deg) translateY(180px);
      }
    }

    /* kuva PUPU osioiden väliin 3-osaisen tilalle*/
    @media screen and (max-width:1000px) {
      .content3 {
        display: block;
        width: 150px;
        height: 150px;
        bottom: -75px;
        position: absolute;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        text-align: center;
      }
    }

    @media screen and (max-width:414px) {
      .content3 {
        padding-top: 2em;
      }
    }

    @media screen and (max-width:285px) {
      .content3 {
        width: 100px;
        height: 100px;
        bottom: -50px;
      }
    }

    @media screen and (max-width:160px) {
      .content3 {
        display: none;
      }
    }

    /*lilalla taustalla*/
    .content2 h1 {
      color: white;
    }

    /*valkoisella taustalla*/
    @media screen and (max-width:1000px) {
      .content2 h1 {
        margin: 0.5em auto;
        color: #271b57;
      }
    }

    /* be first to hear osio valkoinen tausta, alle 1000 lila*/
    .mobiili {
      display: none;
    }

    /*OSIO 3  BE FIRST */
    #col-1_1 {
      background-color: white;
      margin-top: 5em;
      padding: 8em 2em 4em 2em;
      position: relative;
      justify-content: center;
      transform: translateY(-330px);
    }

    @media (max-width:1000px) {
      #col-1_1 {
        background-color: #271b57;
        margin-top: 0;
        margin-bottom: 0;
        width: 100%;
        text-align: center;
        justify-content: center;
        transform: translateY(-120px);
      }
    }


    @media screen and (max-width:600px) {
      #col-1_1 {
        margin: 0;
        padding: 0 2em;
        position: relative;
        bottom: auto;
        left: auto;
        transform: translateY(-2px);
      }
    }

    /*3-OSAINEN SALMIAKKI TEKSTIT*/
    #super {
      position: absolute;
      margin-left: auto;
      margin-right: auto;
      text-align: center;
      top: 2px;
      z-index: 99;
      margin-top: -1em;
      width: 45%;
      padding: 0;
      left: 50%;
      transform: translate(-50%, -50%);
    }

    @media screen and (max-width:1600px) {
      #super {
        width: 60%;
      }
    }

    @media screen and (max-width:1300px) {
      #super {
        display: none;
      }
    }

    .eka {
      display: inline-block;
      background-color: #f78880;
      margin: 2em 1em;
      padding-top: 23px;
      width: 200px;
      height: 150px;
      position: relative;
      transform: skewY(-10deg);
    }

    .toka {
      display: inline-block;
      background-color: #a1d9da;
      margin: 2em 1em;
      padding-top: 23px;
      width: 200px;
      height: 150px;
      position: relative;
      transform: skewY(-10deg);
    }

    .kolmas {
      display: inline-block;
      background-color: #f78880;
      margin: 2em 1em;
      padding-top: 23px;
      width: 200px;
      height: 150px;
      position: relative;
      transform: skewY(-10deg);
    }

    .mustekala {
      width: 300px;
      height: 300px;
      position: absolute;
      left: 20%;
      top: 340px;
    }

    @media screen and (max-width:2350px) {
      .mustekala {
        left: 15%;
      }
    }

    @media screen and (max-width:1800px) {
      .mustekala {
        width: 200px;
        height: 200px;
        top: 390px;
      }
    }

    @media screen and (max-width:1000px) {
      .mustekala {
        width: 160px;
        height: 160px;
        top: 460px;
        left: 5%;
      }
    }

    @media screen and (max-width:780px) {
      .mustekala {
        left: 60%;
        top: 500px;
      }
    }

    @media screen and (max-width:710px) {
      .mustekala {
        top: 540px;
      }
    }

    @media screen and (max-width:600px) {
      .mustekala {
        display: none;
      }
    }

    @media screen and (max-width:550px) {
      .firstjoin {
        display: none;
      }
    }

    @media screen and (max-width: 550px) {
      .mobiili {
        display: inline-block;
        margin-top: 0px;
        margin-bottom: 1em;
      }
    }

    /* OSIO 4 FORM*/
    #col-4 {
      width: 100%;
      margin-top: 0;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    @media (max-width: 500px) {
      #col-4 {
        width: 80%;
        padding: 0;
        margin: 0;
        border: 0px;
      }
    }

    .kummitus {
      width: 150px;
      height: 150px;
      position: absolute;
      margin-right: -500px;
      top: 100px;
      opacity: 0.6;
    }

    @media (max-width: 860px) {
      .kummitus {
        margin: 0;
        display: none;
      }
    }

    #form {
      margin-top: -280px;
      width: 100%;

    }

    @media screen and (max-width: 1000px) {
      #form {
        margin-top: -50px;
        ;
        width: 100%;
        justify-content: center;
        align-items: center;
        font-size: 1rem;
      }
    }


    @media screen and (max-width: 610px) {
      #form {
        margin-top: 0;
        transform: translateY(-2px);
      }
    }

    #taustakuva {
      width: 100%;
      max-width: 3000px;
      height: 800px;
      margin: 0;
      padding: 0;
      overflow: hidden;
      justify-content: center;
      align-items: center;
      align-content: center;
      background-repeat: no-repeat;
      background-size: cover;
      background-position: bottom 0;
    }

    @media screen and (max-width: 1000px) {
      #taustakuva {
        display: none !important;
      }
    }
  </style>


  <?php get_template_part('templates/partial/partial-header'); ?>