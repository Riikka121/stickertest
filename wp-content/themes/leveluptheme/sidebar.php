<div class="col-lg-3 page-sidebar">
  <h3 class="page-subtitle">
    <?php _e('Categories', 'levelup'); ?>
  </h3>
  <ul class="post-categories">
    <?php wp_list_categories(array(
      'title_li' => ''
    )) ?>
  </ul>
</div>