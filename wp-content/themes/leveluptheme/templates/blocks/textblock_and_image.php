<?php

$title = get_field('title');
$content_editor = get_field('content_editor');
$text_align = get_field('text_align');
$image = get_field('image');
$image_position = get_field('image_position');
$button = get_field('button');
$btn    = \Levelup\button($button['button']);

$column_order = $image_position ? ' reverse' : ' normal';

?>

<div class="textblock_and_image__wrapper <?php echo $text_align . $column_order ?>">
  <figure class="textblock_and_image__column textblock_and_image__image" style="background-image:url('<?php echo $image['url'] ?>');">
  </figure>

  <div class="textblock_and_image__column">
    <div class="column__content">
      <?php if ($title) : ?>
        <h2 class="section-title"><?php echo $title ?></h2>
      <?php endif ?>
  
      <?php echo $content_editor ?>
  
      <?php echo $btn ?>
    </div>
  </div>
</div>