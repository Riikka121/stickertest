<?php

$title = get_field('title');
$text_align = get_field('text_align');
$contacts = get_field('contacts');

?>

<div class="contacts">
  <div class="column__content">
    <?php if ($title) : ?>
      <div class="container <?php echo $text_align ?>">
        <h2 class="section-title">
          <?php echo $title ?>
        </h2>
      </div>
    <?php endif ?>

    <?php if ($contacts) : ?>
      <div class="container">
        <div class="row">
          <?php foreach ($contacts as $contact) : ?>
            <div class="col-xl-6 contacts__contact">
              <figure class="contact__image" style="background-image:url('<?php echo $contact['image']['url'] ?>')">

              </figure>

              <div class="contact__info">

                <?php if ($contact['name']) : ?>
                  <h3 class="h5" itemprop="name"><?php echo $contact['name'] ?></h3>
                <?php endif ?>

                <?php if ($contact['title']) : ?>
                  <p class="h6" itemprop="title"><?php echo $contact['title'] ?></p>
                <?php endif ?>

                <?php if ($contact['email']) : ?>
                  <p>
                    <a href="mailto:<?php echo $contact['email'] ?>"><?php echo $contact['email'] ?></a>
                  </p>
                <?php endif ?>

                <?php if ($contact['phone']) : ?>
                  <p>
                    <a href="tel:<?php echo $contact['phone'] ?>"><?php echo $contact['phone'] ?></a>
                  </p>
                <?php endif ?>
              </div>
            </div>
          <?php endforeach ?>
        </div>
      </div>
    <?php endif ?>
  </div>
</div>