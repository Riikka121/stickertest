<?php

$title = get_field('title');
$address_title = get_field('address_title');
$address = get_field('address');
$email_title = get_field('email_title');
$email = get_field('email');
$phone_title = get_field('phone_title');
$phone = get_field('phone');

?>

<div class="container">
  <div class="contact_info__links">
    <div class="column__content">
      <?php if ($title) : ?>
        <h2 class="section-title"><?php echo wp_kses_post($title) ?></h2>
      <?php endif ?>
  
      <?php if ($address) : ?>
        <p class="h3">
          <span class="contact_info__title"><?php esc_html_e($address_title) ?></span>
          <a class="contact_info__link" target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php echo urlencode($address) ?>"><?php echo $address ?></a>
        </p>
      <?php endif ?>
  
      <?php if ($email) : ?>
        <p class="h3">
          <span><?php esc_html_e($email_title) ?></span>
          <a href="mailto:<?php echo $email ?>"><?php echo antispambot($email) ?></a>
        </p>
      <?php endif ?>
      
      <?php if ($phone) : ?>
        <p class="h3">
          <span><?php esc_html_e($phone_title) ?></span>
          <a href="tel:<?php echo $phone ?>"><?php esc_html_e($phone) ?></a>
        </p>
      <?php endif ?>

    </div>
  </div>
</div>