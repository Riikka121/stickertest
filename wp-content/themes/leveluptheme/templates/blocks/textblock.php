<?php

$title = get_field('title');
$content_editor = get_field('content_editor');
$text_align = get_field('text_align');
$block_width = get_field('block_width');
$button = get_field('button');
$btn    = \Levelup\button($button['button']);

$width = $block_width ? ' narrow__textblock' : '';

?>

<div class="container">
  <div class="textblock-wrapper <?php echo $text_align . $width ?>">
    <?php if ($title) : ?>
      <h2 class="section-title"><?php echo $title ?></h2>
    <?php endif ?>

    <?php echo $content_editor ?>

    <?php echo $btn ?>
  </div>
</div>