<?php

$slider = get_field('slider');
$block_id = isset($block) ? $block['id'] : '';

?>

<script type="text/javascript">
  jQuery(function($) {
    $('.hero-slider__<?php echo $block_id ?>').slick({
      dots: true,
      infinite: true,
      pauseOnHover: true,
      speed: 700,
      autoplay: true,
      autoplaySpeed: 4000,
      arrows: false,
      fade: true,
    });
  });
</script>

<?php if ($slider) : ?>
  <div class="hero-slider__wrapper">
    <div class="hero-slider__<?php echo $block_id ?>">
      <?php foreach ($slider as $slide) :

        $btn = \Levelup\button($slide['button']);

        $class = '';

        if ($slide['title'] || $slide['text'] || $btn) {
          $class = 'overlay';
        }

      ?>
        <div class="hero-slider__slide <?php echo $class ?>" style="background-image:url('<?php echo $slide['image']['url'] ?>');">
          <div class="hero-slider__slide__content">
            <div class="container">
              <?php if ($slide['title']) : ?>
                <h2 class="section-title"><?php echo $slide['title'] ?></h2>
              <?php endif ?>

              <?php if ($slide['text']) : ?>
                <p><?php echo $slide['text'] ?></p>
              <?php endif ?>

              <?php echo $btn ?>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>
  </div>
<?php endif ?>