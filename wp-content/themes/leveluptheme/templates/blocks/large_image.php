<?php

$image = get_field('image');
$text = get_field('text');
$text_align = get_field('text_align');
$vertical_align = get_field('vertical_align');
$block_width = get_field('block_width');
$button = get_field('button');
$btn    = \Levelup\button($button['button']);

$justify_content = $vertical_align ? 'justify-content-center' : 'justify-content-end';

?>

<div class="large_image__wrapper">
  <div class="row<?php echo $block_width ? '' : ' no-gutters' ?>">
    <figure class="large_image__bg <?php echo $text ? 'bg-overlay' : '' ?> <?php echo $text_align . ' ' . $justify_content ?>" style="background-image:url('<?php echo $image['url']; ?>');">
    <div class="container">
        <div class="column__content">
          <?php if ($text) : ?>
            <h2 class="section-title"><?php echo $text ?></h2>
          <?php endif ?>

          <?php echo $btn ?>
        </div>
      </div>
    </figure>
  </div>
</div>