<?php

$taxonomy = 'product_cat';
$args = array(
  'taxonomy' => $taxonomy,
  'title_li' => '',
  'hide_empty' => 1,
  'order' => 'asc',
);

$product_cats = get_terms($args);

?>

<div class="sidebar">
  <div class="sidebar-content">

    <div class="product-category-dropdown">
      <button class="product-category__toggler collapsed" data-toggle="collapse" data-target="#product-category-collapse" aria-expanded="false" aria-controls="collapseExample">
        <p class="h3 sidebar-title"><?php _e('Categories', 'levelup'); ?> <span class="toggler__caret"></span></p>
      </button>

      <h3 class="h2 category_sidebar-title sidebar-title"><?php _e('Categories', 'levelup'); ?></h3>
      <div class="product-categories collapse" id="product-category-collapse">
        <?php if ($product_cats) : ?>
          <ul class="wc-product-cat-dropdown">
            <?php wp_list_categories($args); ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>