<a class="header__cart-link" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('Cart', 'levelup'); ?>">

  <?php $product_count = WC()->cart->get_cart_contents_count(); ?>

  <div class="header-icon header-icon__cart">

    <?php if ($product_count > 0) : ?>
      <span class="product_count"><?php echo $product_count; ?></span>
    <?php endif; ?>

  </div>
</a>