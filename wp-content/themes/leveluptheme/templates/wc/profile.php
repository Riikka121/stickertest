<?php

$profile_link = get_permalink(get_option('woocommerce_myaccount_page_id'));

?>

<a class="header__profile-link" href="<?php echo $profile_link ?>" title="<?php _e('Account', 'levelup'); ?>">

  <div class="header-icon header-icon__profile"></div>

</a>