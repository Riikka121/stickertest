<?php
  wp_nav_menu(array(
    'theme_location'    => 'footer',
    'container'       => 'div',
    'container_id'    => 'footer-nav',
    'menu_id'         => false,
    'menu_class'      => 'footer-nav',
    'depth'           => 2,
  ));
