<section class="no-results not-found">

  <header class="page-header">

    <h1 class="page-title"><?php esc_html_e('Nothing Found', 'levelup'); ?></h1>

  </header>

  <div class="block-wrapper page--content">

    <?php if (is_search()) : ?>

      <p><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'levelup'); ?></p>
      <?php
      get_search_form();
    else : ?>

      <p><?php esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'levelup'); ?></p>
      <?php
      get_search_form();
    endif; ?>
  </div>

</section>