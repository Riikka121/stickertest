<?php if (is_single()) : ?>

  <div class="entry__meta">

    <?php \Levelup\get_posted_on(); ?>

    <?php \Levelup\get_post_category_list($post->ID) ?>

    <!-- <?php if ($tag_list) : ?>

      <div class="tag-list">
        <p><?php _e('Tags', 'levelup'); ?></p>
        <?php echo $tag_list ?>
      </div>

    <?php endif; ?> -->

  </div>

<?php endif; ?>