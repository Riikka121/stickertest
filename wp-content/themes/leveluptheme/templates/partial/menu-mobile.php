<?php

wp_nav_menu(array(
  'theme_location'    => 'primary',
  'container'       => 'div',
  'container_id'    => 'mobile_nav',
  'container_class' => 'navbar-wrapper collapse navbar-collapse',
  'menu_id'         => false,
  'menu_class'      => 'navbar-nav',
  'depth'           => 2,
));
