<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <?php get_template_part('templates/partial/entry-title'); ?>

  <div class="block-wrapper <?php echo is_singular('post') ? 'entry' : 'page' ?>--content">

    <?php

    if(!is_single()) {
      the_excerpt();
    } else {
      the_content();
    }


    wp_link_pages(
      array(
        'before' => '<div class="page__links">' . __('Pages:', 'levelup'),
        'after'  => '</div>',
      )
    );

    ?>

  </div>

  <?php get_template_part('templates/partial/entry-meta'); ?>

</article><!-- #post-## -->