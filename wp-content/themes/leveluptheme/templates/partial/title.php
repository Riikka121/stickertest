<div class="page-header">
  <?php

  if(!is_front_page() && is_home()) {
    the_archive_title('<h1 class="page-title">', '</h1>');
  } else {
    the_title('<h1 class="page-title">', '</h1>');
  }

  ?>
</div>
