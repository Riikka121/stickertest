<?php

the_title('<h1 class="d-none">', '</h1>');

?>

<div class="block-wrapper page--content">

  <?php the_content(); ?>

</div>